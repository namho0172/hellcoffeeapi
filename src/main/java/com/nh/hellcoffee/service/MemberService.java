package com.nh.hellcoffee.service;

import com.nh.hellcoffee.entity.Member;
import com.nh.hellcoffee.model.Hell.member.MemberRequest;
import com.nh.hellcoffee.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id){
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberRequest request){
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setSpending(request.getSpending());
        addData.setTotal(request.getTotal());

        memberRepository.save(addData);
    }
}
