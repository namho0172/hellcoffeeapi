package com.nh.hellcoffee.service;

import com.nh.hellcoffee.entity.Hell;
import com.nh.hellcoffee.entity.Member;
import com.nh.hellcoffee.model.Hell.HellRequest;
import com.nh.hellcoffee.repository.HellRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class HellService {
    private final HellRepository hellRepository;

    public void setHell(Member member, HellRequest request){
        Hell addData = new Hell();
        addData.setMember(member);
        addData.setTotalCost(addData.getTotalCost());
        addData.setDateCost(LocalDate.now());

        hellRepository.save(addData);
    }
}
