package com.nh.hellcoffee.controller;

import com.nh.hellcoffee.model.Hell.member.MemberRequest;
import com.nh.hellcoffee.service.HellService;
import com.nh.hellcoffee.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;
    private final HellService hellService;

    @PostMapping("/join")
    public String setMember(@RequestBody MemberRequest request){
        memberService.setMember(request);

        return "Ok";
    }

}
