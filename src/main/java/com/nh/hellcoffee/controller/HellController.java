package com.nh.hellcoffee.controller;

import com.nh.hellcoffee.entity.Member;
import com.nh.hellcoffee.model.Hell.HellRequest;
import com.nh.hellcoffee.service.HellService;
import com.nh.hellcoffee.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/hell")
public class HellController {
    private final MemberService memberService;
    private final HellService hellService;

    @PostMapping("/new/member-id/{memberId}")
    public String setHell(@PathVariable long memberId, @RequestBody HellRequest request){
        Member member = memberService.getData(memberId);
        hellService.setHell(member, request);

        return "OK";
    }
}
