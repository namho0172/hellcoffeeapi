package com.nh.hellcoffee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HellCoffeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(HellCoffeeApplication.class, args);
	}

}
