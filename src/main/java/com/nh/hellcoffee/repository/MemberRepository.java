package com.nh.hellcoffee.repository;

import com.nh.hellcoffee.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
