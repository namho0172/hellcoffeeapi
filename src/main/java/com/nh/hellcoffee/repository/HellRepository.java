package com.nh.hellcoffee.repository;

import com.nh.hellcoffee.entity.Hell;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HellRepository extends JpaRepository<Hell, Long> {
}
