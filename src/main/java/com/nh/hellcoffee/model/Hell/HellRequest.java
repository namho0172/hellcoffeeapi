package com.nh.hellcoffee.model.Hell;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class HellRequest {
    private String totalCost;
    private Double dateCost;
}
