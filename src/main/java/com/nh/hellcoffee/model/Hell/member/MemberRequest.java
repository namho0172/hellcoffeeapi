package com.nh.hellcoffee.model.Hell.member;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MemberRequest {
    private String name;
    private String phoneNumber;
    private Double spending;
    private Long total;
}
